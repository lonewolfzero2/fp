/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Entity.Document;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.border.Border;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

/**
 *
 * @author WINDA
 */
public class Details extends javax.swing.JFrame {

    /**
     * Creates new form ComparationDetails
     */
    public Details() {
        initComponents();
    }
    
    public Details(Document doc1, Document doc2, double persentaseKeseluruhan) {
        initComponents();
        
        this.setLocationRelativeTo(null); // supaya muncul di tengah
        
        // set judul dokumen
        jPanelDokumen1.setBorder(javax.swing.BorderFactory.createTitledBorder(doc1.getJudul()));
        jPanelDokumen2.setBorder(javax.swing.BorderFactory.createTitledBorder(doc2.getJudul()));
        
        doc1.setKgramSuspected();
        doc2.setKgramSuspected();

        String[] kgramSama = new String[doc1.getKgramSuspected().length];
        int x = 0;
        for(String ks : doc1.getKgramSuspected()){
            for(String ko : doc2.getKgramSuspected()){
                if (ks.equals(ko)) {
                    kgramSama[x] = ks;
                    x++;
                    break;                    
                }
            }
        }

        warnaiKgram(doc1, kgramSama, jTextPane1);
        warnaiKgram(doc2, kgramSama, jTextPane2);
    }
    
    public Details(Document doc1, Document doc2, double persentaseParagraf, double[][] resultsPerParagraf) {
        initComponents();
        
        this.setLocationRelativeTo(null); // supaya muncul di tengah
        
        // set judul dokumen
        jPanelDokumen1.setBorder(javax.swing.BorderFactory.createTitledBorder(doc1.getJudul()));
        jPanelDokumen2.setBorder(javax.swing.BorderFactory.createTitledBorder(doc2.getJudul()));
        
        warnaiParagraf(doc1, doc2, resultsPerParagraf, jTextPane1, jTextPane2);
    }
    
    private void warnaiKgram(Document document, String[] kgramSama, javax.swing.JTextPane jTextPane) {
        StyledDocument doc = jTextPane.getStyledDocument();
        
        Style styleRed = jTextPane.addStyle("RED", null);
        StyleConstants.setForeground(styleRed, Color.red);
        
        Style styleBlack = jTextPane.addStyle("BLACK", null);
        StyleConstants.setForeground(styleBlack, Color.black);
        
        int batasDiwarnai = 5000;
        
        if(batasDiwarnai > document.getContent().length())
            batasDiwarnai = document.getContent().length() - 5;
        
        for(int i=0; i<batasDiwarnai; i++) {
            String ngram = "";
            int j = i;
            while (ngram.length() < 5) {
                if(document.getContent().charAt(j) != ' ') {
                    ngram = ngram + document.getContent().toLowerCase().charAt(j);
                }
                j++;
            }
            
            boolean kgramAda = false;
            for(int k=0; k<kgramSama.length; k++) if(ngram.equals(kgramSama[k])) kgramAda = true;
            
            if(kgramAda) {
                try {
                    int l = i;
                    int counter = 0;
                    while (counter < 5) {
                        String huruf = String.valueOf(document.getContent().charAt(l));
                        doc.insertString(doc.getLength(), huruf, styleRed);
                        if(document.getContent().charAt(l) != ' ') {
                            counter++;
                        }
                        l++;
                    }
                    i = l - 1;
                }
                catch(BadLocationException e) {
                    System.out.println(e.getMessage());
                }
            }
            else {
                try {
                    String huruf = String.valueOf(document.getContent().charAt(i));
                    doc.insertString(doc.getLength(), huruf, styleBlack);
                }
                catch(BadLocationException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
        
        for(int i=batasDiwarnai; i<document.getContent().length(); i++) {
            String huruf = String.valueOf(document.getContent().charAt(i));
            try {
                doc.insertString(doc.getLength(), huruf, styleBlack);
            }
            catch(BadLocationException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private void warnaiParagraf(Document doc1, Document doc2, double[][] resultPerParagraf, javax.swing.JTextPane jTextPane1, javax.swing.JTextPane jTextPane2) {
        StyledDocument styledDocument1 = jTextPane1.getStyledDocument();
        StyledDocument styledDocument2 = jTextPane2.getStyledDocument();
        
        Style styleRed1 = jTextPane1.addStyle("RED", null);
        StyleConstants.setForeground(styleRed1, Color.red);
        Style styleRed2 = jTextPane2.addStyle("RED", null);
        StyleConstants.setForeground(styleRed2, Color.red);
        
        Style styleBlack1 = jTextPane1.addStyle("BLACK", null);
        StyleConstants.setForeground(styleBlack1, Color.black);
        Style styleBlack2 = jTextPane2.addStyle("BLACK", null);
        StyleConstants.setForeground(styleBlack2, Color.black);
        
        int[] flagDokumen1 = new int[doc1.getFingerprintParagraf().size()];
        int[] flagDokumen2 = new int[doc2.getFingerprintParagraf().size()];
        
        for (int i = 0; i<doc1.getFingerprintParagraf().size(); i++) {
            for (int j = 0; j<doc2.getFingerprintParagraf().size(); j++) {
//                if(resultPerParagraf[i][j] > 50 && doc1.getParagraf()[i].split(" ").length > 1) {
//                    if(flagDokumen1[i] == 0) flagDokumen1[i] = 1;
//                }
//                
//                if(resultPerParagraf[i][j] > 50 && doc2.getParagraf()[j].split(" ").length > 1) {
//                    if(flagDokumen2[j] == 0) flagDokumen2[j] = 1;
//                }
                
                if(resultPerParagraf[i][j] > 50) {
                    if(flagDokumen1[i] == 0) flagDokumen1[i] = 1;
                    if(flagDokumen2[j] == 0) flagDokumen2[j] = 1;
                }
            }
        }
        
        // warnai dokumen 1
        for(int i=0; i<flagDokumen1.length; i++) {
            if(flagDokumen1[i] == 1) {
                try {
                    String huruf = String.valueOf(doc1.getParagraf()[i]) + "\n";
                    styledDocument1.insertString(styledDocument1.getLength(), huruf, styleRed1);
                }
                catch(BadLocationException e) {
                    System.out.println(e.getMessage());
                }
            }
            else {
                try {
                    String huruf = String.valueOf(doc1.getParagraf()[i]) + "\n";
                    styledDocument1.insertString(styledDocument1.getLength(), huruf, styleBlack1);
                }
                catch(BadLocationException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
        
        // warnai dokumen 2
        for(int j=0; j<flagDokumen2.length; j++) {
            if(flagDokumen2[j] == 1) {
                try {
                    String huruf = String.valueOf(doc2.getParagraf()[j]) + "\n";
                    styledDocument2.insertString(styledDocument2.getLength(), huruf, styleRed2);
                }
                catch(BadLocationException e) {
                    System.out.println(e.getMessage());
                }
            }
            else {
                try {
                    String huruf = String.valueOf(doc2.getParagraf()[j]) + "\n";
                    styledDocument2.insertString(styledDocument2.getLength(), huruf, styleBlack2);
                }
                catch(BadLocationException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabelJudul = new javax.swing.JLabel();
        jPanelDokumen1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextPane1 = new javax.swing.JTextPane();
        jPanelDokumen2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextPane2 = new javax.swing.JTextPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Plagiarism Detection: Detail Perbandingan");
        setMaximumSize(new java.awt.Dimension(665, 640));
        setMinimumSize(new java.awt.Dimension(665, 640));
        setPreferredSize(new java.awt.Dimension(665, 640));
        setResizable(false);

        jLabelJudul.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabelJudul.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelJudul.setText("Detail Perbandingan");

        jPanelDokumen1.setBorder(javax.swing.BorderFactory.createTitledBorder("Dokumen 1"));

        jScrollPane1.setViewportView(jTextPane1);

        javax.swing.GroupLayout jPanelDokumen1Layout = new javax.swing.GroupLayout(jPanelDokumen1);
        jPanelDokumen1.setLayout(jPanelDokumen1Layout);
        jPanelDokumen1Layout.setHorizontalGroup(
            jPanelDokumen1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE)
        );
        jPanelDokumen1Layout.setVerticalGroup(
            jPanelDokumen1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
        );

        jPanelDokumen2.setBorder(javax.swing.BorderFactory.createTitledBorder("Dokumen 2"));

        jScrollPane2.setViewportView(jTextPane2);

        javax.swing.GroupLayout jPanelDokumen2Layout = new javax.swing.GroupLayout(jPanelDokumen2);
        jPanelDokumen2.setLayout(jPanelDokumen2Layout);
        jPanelDokumen2Layout.setHorizontalGroup(
            jPanelDokumen2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE)
        );
        jPanelDokumen2Layout.setVerticalGroup(
            jPanelDokumen2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 562, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelJudul, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanelDokumen1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanelDokumen2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelJudul)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanelDokumen2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanelDokumen1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Details.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Details.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Details.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Details.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Details().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabelJudul;
    private javax.swing.JPanel jPanelDokumen1;
    private javax.swing.JPanel jPanelDokumen2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextPane jTextPane1;
    private javax.swing.JTextPane jTextPane2;
    // End of variables declaration//GEN-END:variables
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Entity.Document;
import Views.Details;
import Views.OneToMany;
import Views.OneToOne;
import java.util.ArrayList;
/**
 *
 * @author WINDA
 */
public class FPWControl {
    
    public FPWControl() {
        
    }
    
    public void runOneToOne() {
        new OneToOne().setVisible(true);
    }
    
    public void runOneToMany() {
        new OneToMany().setVisible(true);
    }
    
    public void runDetails(Document doc1, Document doc2, double persentase) {
        new Details(doc1, doc2, persentase).setVisible(true);
    }
    
    public void runDetails(Document doc1, Document doc2, double persentase, double[][] results) {
        new Details(doc1, doc2, persentase, results).setVisible(true);
    }
    
    public double compareOneToOneKeseluruhan(Document doc1, Document doc2) {
        String[] doc1Fingerprint = doc1.getFingerprintSeluruh().split("\n");
        String[] doc2Fingerprint = doc2.getFingerprintSeluruh().split("\n");
        //System.out.println(doc1.getFingerprintSeluruh());
        
        double hasil = 0;
        for (String x : doc1Fingerprint) {
            for (String y : doc2Fingerprint) {
                if (x.equals(y)) {
                    hasil++;
                    break;
                }
            }
        }

        double result = (double) hasil / (double) (doc1Fingerprint.length + doc2Fingerprint.length - hasil) * 100;
        return result;
    }
    
    public double[][] compareOneToOnePerParagraf(Document doc1, Document doc2) {
        double[][] result = new double[doc1.getFingerprintParagraf().size()][doc2.getFingerprintParagraf().size()];
        
        for(int i=0; i<doc1.getFingerprintParagraf().size(); i++) {
            for(int j=0; j<doc2.getFingerprintParagraf().size(); j++) {
                String[] doc1Fingerprint = doc1.getFingerprintParagraf().get(i).split("\n");
                String[] doc2Fingerprint = doc2.getFingerprintParagraf().get(j).split("\n");

                int hasil = 0;
                for (String x : doc1Fingerprint) {
                    for (String y : doc2Fingerprint) {
                        if (x.equals(y)) {
                            hasil++;
                            break;
                        }
                    }
                }
                
                int panjangFingerprint = doc1.getFingerprintParagraf().get(i).split("\n").length;
                result[i][j] = ((double) hasil / (double) panjangFingerprint) * 100;
                
//                System.out.println(i + " " + j + " " + result[i][j] + " " + hasil + " " + panjangFingerprint);
//                System.out.println(doc1.getParagraf()[i]);
//                System.out.println(doc2.getParagraf()[j]);
            }
        }
        
        return result;
    }
    
    public double persentaseOneToOnePerParagraf(Document doc1, Document doc2, double[][] result) {
        int jumlahKataYangSama = 0;
        
        int paragrafSuspected;
        for(int i=0; i<doc1.getFingerprintParagraf().size(); i++) {
            paragrafSuspected = -1;            
            for(int j=0; j<doc2.getFingerprintParagraf().size(); j++) {
                // jika jumlah fingerprint yang sama lebih dari 50%
                if(result[i][j] >= 50 && doc1.getParagraf()[i].replace("  ", " ").split(" ").length > 1) {
                    paragrafSuspected = i;
                }
            }
            
            if(paragrafSuspected != -1) {
                jumlahKataYangSama += doc1.getParagraf()[i].replace("  ", " ").split(" ").length;
            }
        }
        
        String[] dokParagraf = doc1.getContent().split("\n");
        int jum = 0;
        int x = 0;
        for(int i=0; i<dokParagraf.length; i++) {
            jum += dokParagraf[i].replace("  ", " ").split(" ").length;
        }
        int jumlahKataDokumen = jum;
        
        //System.out.println(jumlahKataYangSama + " " + jumlahKataDokumen);
        
        double persentase = ((double) jumlahKataYangSama / (double) jumlahKataDokumen) * 100;
        return persentase;
    }
    
    public double[] compareOneToManyKeseluruhan(Document suspectedDoc, ArrayList<Document> docOrigins) {
        double[] results =  new double[docOrigins.size()];
        
        for(int i=0; i<docOrigins.size(); i++) {
            results[i] = this.compareOneToOneKeseluruhan(suspectedDoc, docOrigins.get(i));
        }
        
        return results;
    }
    
    public ArrayList<double[][]> compareOneToManyPerParagraf(Document suspectedDoc, ArrayList<Document> docOrigins) {
        ArrayList<double[][]> results =  new ArrayList<double[][]>();
        
        for(int i=0; i<docOrigins.size(); i++) {
            results.add(this.compareOneToOnePerParagraf(suspectedDoc, docOrigins.get(i)));
        }
        
        return results;
    }
    
    public double[] persentaseOneToManyPerParagraf(Document suspectedDoc, ArrayList<Document> docOrigins, ArrayList<double[][]> result) {
        double[] persentase =  new double[docOrigins.size()];
        
        for(int i=0; i<docOrigins.size(); i++) {
            double[][] resultPerParagraf = this.compareOneToOnePerParagraf(suspectedDoc, docOrigins.get(i));
            persentase[i] = this.persentaseOneToOnePerParagraf(suspectedDoc, docOrigins.get(i), resultPerParagraf);
        }
        
        return persentase;
    }
}

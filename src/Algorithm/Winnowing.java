/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Algorithm;
import java.util.ArrayList;


/**
 *
 * @author wnd
 */
public class Winnowing {
    
    private int[] hash;
    
    public ArrayList<int[]> windowMin = new ArrayList<int[]>();
    private ArrayList<int[]> windows = new ArrayList<int[]>();
    
    private int[] window = new int[5];
    private int[] minvalue = new int[2];
    public Winnowing(int[] Hash)
    {
        hash = Hash;
    }
    public void Bagi()
    {                
        int min = 99999999;
        int posisi;
        int tempmin = 99999999;
        for(int i=0;i<hash.length-5;i++)
        {
            for(int j=0;j<5;j++)
            {
                window[j] = hash[i+j];  
                if(window[j]<min)
                {
                    min = window[j];  
                    minvalue[0] = min;
                    minvalue[1]=i+j;
                }
            }
            
            if(min!=tempmin)
            {
                windowMin.add(minvalue);
                windows.add(window);
            }
            
            tempmin = min;
            min = 99999999;
            
            minvalue = new int[2];
            window = new int[5];
        }
    }
    
    public ArrayList<int[]> getWindows()
    {
        return windows;       
    }
    
    public ArrayList<int[]> getWindowMin()
    {
        return windowMin;
    }
    
    public String HasilWinnowing()
    {
        String temp = "";
        int[] window = new int[5];
        int x = 0;
        int xx = 0;
        
        for(int i=0; i<windows.size(); i++) {            
            window = windows.get(i);
            /*temp+="{ ";
            for(int j=0;j<5;j++)
            {                
                temp+=String.valueOf(window[j])+" ";
                
            }
            temp+="}";*/
            temp+=" "+windowMin.get(i)[0] + "\n";
        }
        
        return temp;
    }
    
    public int[] Posisi()
    {
        int result[] = new int[windows.size()];
        for (int i=0; i<windows.size(); i++) {
            result[i] = windowMin.get(i)[1];
         }
        return result;
    }
    
    public String HasilPosisi()
    {
        String posisi = "";
        int [] window = new int [5];
        int a = 0;
        int aa = 0;
        
        for (int i=0; i<windows.size(); i++) {
            window = windows.get(i);
            posisi+=" "+windowMin.get(i)[1] + "\n";
         }
        return posisi;
    }
    
    public String BagiHashing()
    {
        String temp = "";
        int[] window = new int[5];
        int x = 0;
        int xx = 0;
        
        for(int i=0; i<windows.size(); i++) {            
            window = windows.get(i);
            temp+="{ ";
            for(int j=0;j<5;j++)
            {                
                temp+=String.valueOf(window[j])+" ";
                
            }
            temp+="}";
            temp+="\n";
        }
        
        return temp;
    }
}

    
    


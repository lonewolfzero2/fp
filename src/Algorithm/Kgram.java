/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Algorithm;

/**
 *
 * @author wnd
 */
public class Kgram {
    
    public String[] kgram(String input, int k) {
        if(input.length() < k) {
            String[] hasil = new String[1];
            hasil[0] = input;
            return hasil;
        }
        else {
            String[] hasil = new String[input.length()-k+1];
            for (int i=0; i<input.length() - k + 1; i++) {
                hasil[i] = "";
                for (int j=i; j<i+k; j++) {
                    hasil[i] += String.valueOf(input.charAt(j));
                }
            }
            return hasil;
        }
    }
    
}
